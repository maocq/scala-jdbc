package com.maocq.scala

import java.sql.{Connection, DriverManager}

object Main {

  val url = "jdbc:mysql://localhost:3306/db_jdbc?zeroDateTimeBehavior=convertToNull"
  val driver = "com.mysql.jdbc.Driver"
  val username = "root"
  val password = ""
  var connection: Connection = _

  def main(args: Array[String]): Unit = {

    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)

      val sql: String = "SELECT * FROM users"
      val statement = connection.prepareStatement(sql)

      val rs = statement.executeQuery()

      var usuarios: List[User] = List()
      while (rs.next) {
        val id = rs.getInt("id")
        val name = rs.getString("name")
        val email = rs.getString("email")
        val user = User(id, name, email)

        usuarios = user :: usuarios
      }

      if (usuarios.nonEmpty)
        println(usuarios.head.name)

    } catch {
      case e: Exception => e.printStackTrace
    }
    connection.close

  }
}
