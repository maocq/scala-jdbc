package com.maocq.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String url = "jdbc:mysql://localhost:3306/db_jdbc?zeroDateTimeBehavior=convertToNull";
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String username = "root";
    private static final String password = "";

    public static void main(String[] args) {
        Connection connection = null;
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);

            String sql = "SELECT * FROM users";
            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();

            List<User> usuarios = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("name");
                String email = rs.getString("email");
                User usuario = new User(id, nombre, email);

                usuarios.add(usuario);
            }
            if (!usuarios.isEmpty())
                System.out.println(usuarios.get(0).getName());

        } catch (ClassNotFoundException ex) {
            System.out.println("Error Class " + ex.getMessage());
        } catch (SQLException ex) {
            System.out.println("Error SQL " + ex.getMessage());
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    }

}
